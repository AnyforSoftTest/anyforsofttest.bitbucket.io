$(document).ready(function(){
    $('.slick-slider').slick({
    });

    //change opacity of top menu on scroll event
    $(window).scroll(function(){
        if ($(this).scrollTop() > 0){
            $('header .top-menu .bg-color').css('opacity', '1');
        }else{
            $('header .top-menu .bg-color').css('opacity', '0');
        }
    });

    //make hamburger to become a cross onclick event
    $(document).on('click', '.hamburger', function(){
        $('.hamburger span.first').toggleClass('rotate-first');
        $('.hamburger span.second').toggleClass('viz');
        $('.hamburger span.third').toggleClass('rotate-third');
        if($('.hamburger span.second').hasClass('viz')){
            $('header.top .menu nav ul').css('opacity','1');
        }else{
            $('header.top .menu nav ul').css('opacity','0');
        }
    });

        //find height of header inner elements and set header height
        var windowWidth = $(window).width();
        if (windowWidth <= 700){
            setHeaderHeight();
        }
        function setHeaderHeight(){
            var item1 = $('.top .header-content div h1').outerHeight() + 10;
            var item2 = $('.top .header-content div p').outerHeight() + 45;
            var item3 = $('.top .header-content div a').outerHeight() + 50;
            $('header.top').css('height', item1 + item2 + item3 + 80 );
            $('.top .header-content').css('margin-top', '40px' );
        }

        $( window ).resize(function() {
            if (windowWidth <= 700){
                setHeaderHeight();
            }
        });

        $( window ).resize(function() {
            var windowWidth = $(this).outerWidth();

            if (windowWidth >= 768){
                if ($('header.top .menu nav ul').css('opacity') == '0'){
                    $('header.top .menu nav ul').css('opacity','1');
                }
            } else if (windowWidth < 768){
                if ($('header.top .menu nav ul').css('opacity') == '1'){
                    $('header.top .menu nav ul').css('opacity','0');
                    $('.hamburger span.first').removeClass('rotate-first');
                    $('.hamburger span.second').removeClass('viz');
                    $('.hamburger span.third').removeClass('rotate-third');
                }
            }
        });

        //to top button
        $(function() {
            $(window).scroll(function() {
                if($(this).scrollTop() >= 50) {
                    $('#toTop').fadeIn();
                } else {
                $('#toTop').fadeOut();
                }
            });
                $('#toTop').click(function() {
                $('body,html').animate({scrollTop:0},800);
            });
        });

});

